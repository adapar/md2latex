#!/bin/bash

# Mike Hannibal 6 March 2013 v1.0

# A script to convert a document from Markdown to PDF.
# This version runs multiple passes of PDFLaTeX and Biber.
# Designed principally to deal with APA style sheet documents.
# Can be modified for other types.

# Modifications by Andrés Aparicio

# Set up variables.

shorttitle=$(cat shorttitle.md)
abstract=$(cat abstract.md)
keywords=$(cat keywords.md)
affiliations=$(cat affiliations.md)

tex_template=apa6template
csl_template=apa

order="/usr/local/bin/pandoc --data-dir=. -s -f markdown+tex_math_double_backslash --filter=/usr/local/bin/pandoc-citeproc --bibliography bibliography.bib --csl "$csl_template".csl --template="$tex_template".tex --variable shorttitle="\"$shorttitle\"" --variable abstract="\"$abstract\"" --variable keywords="\"$keywords\"" --variable affiliations="\"$affiliations\"" body.md -o body.tex"
echo "Converting to .tex for file - body.md"
echo $order
eval "$order"
