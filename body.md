---
suppress-bibliography: false
reference-section-title: References
title:  'This is the title: it contains a colon'
shorttitle: 'Shorty'
author:
- name: Author One
  affiliation: University of Somewhere
- name: Author Two
  affiliation: University of Nowhere
---

# Introduction

This is a completely fantastic introduction to all sorts of interesing stuff.

Intro in here and here and here
Intro in here and here and here
Intro in here and here and here
Intro in here and here and here
Intro in here and here and here
Intro in here and here and here
Intro in here and here and here
Intro in here and here and here
Intro in here and here and here @cook2005school.
[@jacucci2014symbiotic]. You can also say [@christensen2006robobraille; @brodwin2004computer] where Karmiloff-Smith says whatever [-@karmiloff1998development]. However, @cook2014assistive says other things.

# Method

Method here and here and here and here
Method here and here and here and here
Method here and here and here and here
Method here and here and here and here
Method here and @jacucci2014symbiotic here and here and here
Method here and here and here and here
Method here and here and @farran2011neurodevelopmental here and here
Method here and here and here and here, see Table \ref{my-label}

\begin{table}[]
\centering
\caption{My caption}
\label{my-label}
\begin{tabular}{lcllr}
\hline
asdf & \multicolumn{1}{l}{asdf} & asdf & as                                                                               & \multicolumn{1}{l}{sdf} \\ \hline
asdf & 23                       & 23,4 & adsasdf                                                                          & 2                       \\
asdf & 23                       & 23.2 & \begin{tabular}[c]{@{}l@{}}adsfafsdasd\\ fasdf\\ as\\ df\\ asdf\\ a\end{tabular} & xx                      \\
asdf & 2                        & 2.2  & \begin{tabular}[c]{@{}l@{}}asdfasdf\\ s\end{tabular}                             & x                       \\ \hline
\end{tabular}
\end{table}

## Participants

Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here @cook2014assistive and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here
Details of participants here and here and here and here

## Materials

Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other Bucket loads of satin, rayon and other materials in here materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here.

A reference to the figure \ref{mylabel2}. Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials in here Bucket loads of satin, rayon and other materials See . in here 

[image]: figure_1.png "Image Title 2"
![Alt text \label{mylabel2}][image]

## Design

Blah (Table \ref{table1})

-------------------------------------------------------------
 Centered   Default           Right Left
  Header    Aligned         Aligned Aligned
----------- ------- --------------- -------------------------
   First    row                12.0 Example of a row that
                                    spans multiple lines.

  Second    row                 5.0 Here's another one. Note
                                    the blank line between
                                    rows. 
-------------------------------------------------------------

: Here's the caption. It, too, may span
multiple lines. \label{table1}

## Procedure

Method or methods in here

# Results

Results in here (Table \ref{table2})

-------------------------------------------------------------
 Centered   Default           Right Left
  Header    Aligned         Aligned Aligned
----------- ------- --------------- -------------------------
   First    row                12.0 Example of a row that
                                    spans multiple lines.

  Second    row                 5.0 Here's another one. Note
                                    the blank line between
                                    rows. 
                                    Holy cow!
-------------------------------------------------------------

: Here's the caption. It, too, may span
multiple lines. \label{table2}

# Discussion

Discussion in here

\newpage

\setlength{\parindent}{-0.5in}
\setlength{\leftskip}{0.5in}
\setlength{\parskip}{8pt}

<div id="references"></div>
