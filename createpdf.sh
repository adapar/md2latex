#!/bin/bash

# Mike Hannibal 6 March 2013 v1.0

# A script to convert a document from Markdown to PDF.
# This version runs multiple passes of PDFLaTeX and Biber.
# Designed principally to deal with APA style sheet documents.
# Can be modified for other types.

# Set up variables.

shorttitle=$(cat shorttitle.md)
abstract=$(cat abstract.md)
keywords=$(cat keywords.md)
affiliations=$(cat affiliations.md)

# Create the text file
filename="body"


#/usr/local/bin/pandoc --data-dir=. -s -f markdown+tex_math_double_backslash --latex-engine=/Library/TeX/texbin/pdflatex --filter=/usr/local/bin/pandoc-citeproc --bibliography bibliography.bib --csl apa.csl --template=apa6template.tex -o body.pdf body.md
#/usr/local/bin/pandoc --data-dir=. -s -f markdown+tex_math_double_backslash --filter=/usr/local/bin/pandoc-citeproc --bibliography bibliography.bib --csl apa.csl --template=apa6template.tex -o body.tex body.md
#/usr/local/bin/pandoc --data-dir=. -s -f markdown+tex_math_double_backslash --biblatex --bibliography bibliography.bib --csl apa.csl --template=apa6template.tex -o body.tex
#/usr/local/bin/pandoc --data-dir=. -s -f markdown+tex_math_double_backslash --biblatex --bibliography bibliography.bib --csl apa.csl --template=apa6template.tex body.md -o body.tex

#/usr/local/bin/pandoc --data-dir=. -s -f markdown+tex_math_double_backslash --latex-engine=/Library/TeX/texbin/pdflatex --filter=/usr/local/bin/pandoc-citeproc --bibliography bibliography.bib --csl springer-lecture-notes-in-computer-science.csl --template=llncs_template.tex -o body.pdf body.md
#/usr/local/bin/pandoc --data-dir=. -s -f markdown+tex_math_double_backslash --filter=/usr/local/bin/pandoc-citeproc --bibliography bibliography.bib --csl apa.csl --template=apa6template.tex -o body.tex body.md

#/usr/local/bin/pandoc --data-dir=. -s -f markdown+tex_math_double_backslash --latex-engine=/Library/TeX/texbin/pdflatex --filter=/usr/local/bin/pandoc-citeproc --bibliography bibliography.bib --csl apa.csl --template=apa6template.tex -o body.pdf body.md

#/usr/local/bin/pandoc --data-dir=. -s -f markdown+tex_math_double_backslash --latex-engine=/Library/TeX/texbin/pdflatex --filter=/usr/local/bin/pandoc-citeproc --bibliography bibliography.bib --csl apa.csl --template=apa6template.tex -o --reference-docx=apa6.docx body.docx body.md

# working
#/usr/local/bin/pandoc --data-dir=. -s -f markdown+tex_math_double_backslash --latex-engine=/Library/TeX/texbin/pdflatex --filter=/usr/local/bin/pandoc-citeproc --bibliography bibliography.bib --csl springer-lecture-notes-in-computer-science.csl --template=llncs_template.tex -o body.pdf body.md

order="/usr/local/bin/pandoc --data-dir=. -s -f markdown+tex_math_double_backslash --template=apa6template.tex --variable shorttitle="\"$shorttitle\"" --variable abstract="\"$abstract\"" --variable keywords="\"$keywords\"" --variable affiliations="\"$affiliations\"" body.md -o body.tex"
order="/usr/local/bin/pandoc --data-dir=. -s -f markdown+tex_math_double_backslash --filter=/usr/local/bin/pandoc-citeproc --bibliography bibliography.bib --template=apa6template.tex --variable shorttitle="\"$shorttitle\"" --variable abstract="\"$abstract\"" --variable keywords="\"$keywords\"" --variable affiliations="\"$affiliations\"" "$filename".md -o "$filename".tex"
order="/usr/local/bin/pandoc --data-dir=. -s -f markdown+tex_math_double_backslash --filter=/usr/local/bin/pandoc-citeproc --bibliography bibliography.bib --template=apa6template.tex --variable shorttitle="\"$shorttitle\"" --variable abstract="\"$abstract\"" --variable keywords="\"$keywords\"" --variable affiliations="\"$affiliations\"" "$filename".md -o "$filename".tex"
echo "Converting to .tex for file - $filename.md"
echo $order
eval "$order"

order="pdflatex "$filename".tex"
order2="biber "$filename				

echo "++++++++++++++++++++++++++++++"
echo "PDF to be created with the name: "$filename".pdf"
echo "With the following command:"
echo "$order"
echo "++++++++++++++++++++++++++++++"
echo "PLEASE check immediately below for error messages"
echo "++++++++++++++++++++++++++++++"

$order
$order
# $order2
# $order
# $order2
# $order
