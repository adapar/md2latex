#!/bin/bash

# Mike Hannibal 23 April 2013 v1.0

# Clean up a tex directory after a PDF has been created

mv body.md _body.md

rm *.bbl
rm *.bcf
rm *.blg
rm *.log
rm *.run.xml
rm *.ttt
rm *.fls
rm *.fdb_latexmk
rm *.dvi
rm *.out
rm *.aux
rm *.pdf
rm *.4*
rm *.odt
rm *.html
rm *.css
rm *.tmp
rm *.fff
rm body.*

mv _body.md body.md